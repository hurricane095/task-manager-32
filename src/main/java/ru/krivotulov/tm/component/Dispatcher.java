package ru.krivotulov.tm.component;

import ru.krivotulov.tm.api.endpoint.Operation;
import ru.krivotulov.tm.dto.request.AbstractRequest;
import ru.krivotulov.tm.dto.response.AbstractResponse;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Dispatcher
 *
 * @author Aleksey_Krivotulov
 */
public class Dispatcher {

    private final Map<Class<? extends AbstractRequest>, Operation<?, ?>> map = new LinkedHashMap<>();

    public <RQ extends AbstractRequest, RS extends AbstractResponse> void registry(
            Class<RQ> regClass, Operation<RQ, RS> operation
    ) {
        map.put(regClass, operation);
    }

    public Object call(final AbstractRequest request) {
        final Operation operation = map.get(request.getClass());
        return operation.execute(request);
    }

}
