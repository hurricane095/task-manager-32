package ru.krivotulov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.krivotulov.tm.api.repository.IProjectRepository;
import ru.krivotulov.tm.api.repository.ITaskRepository;
import ru.krivotulov.tm.api.service.IProjectTaskService;
import ru.krivotulov.tm.exception.entity.ProjectNotFoundException;
import ru.krivotulov.tm.exception.entity.TaskNotFoundException;
import ru.krivotulov.tm.exception.field.UserIdEmptyException;
import ru.krivotulov.tm.model.Task;
import ru.krivotulov.tm.util.ListUtil;

import java.util.List;
import java.util.Optional;

public class ProjectTaskService implements IProjectTaskService {

    @NotNull
    private final IProjectRepository projectRepository;

    @NotNull
    private final ITaskRepository taskRepository;

    public ProjectTaskService(@NotNull final IProjectRepository projectRepository,
                              @NotNull final ITaskRepository taskRepository
    ) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Override
    public void bindTaskToProject(@Nullable final String userId,
                                  @Nullable final String projectId,
                                  @Nullable final String taskId
    ) {
        if(userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if(projectId == null || projectId.isEmpty()) return;
        if(taskId == null || taskId.isEmpty()) return;
        if(!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
        @Nullable final Task task = taskRepository.findOneById(userId, taskId);
        if(task == null) throw new TaskNotFoundException();
        task.setProjectId(projectId);
    }

    @Override
    public void unbindTaskFromProject(@Nullable final String userId,
                                      @Nullable final String taskId,
                                      @Nullable final String projectId
    ) {
        if(userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if(projectId == null || projectId.isEmpty()) return;
        if(taskId == null || taskId.isEmpty()) return;
        if(!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
        @Nullable final Task task = taskRepository.findOneById(userId, taskId);
        if(task == null) throw new TaskNotFoundException();
        task.setProjectId(null);
    }

    @Override
    public void removeProjectById(@Nullable final String userId,
                                  @Nullable final String projectId) {
        if(userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if(projectId == null || projectId.isEmpty()) return;
        @NotNull final List<Task> taskList = taskRepository.findAllByProjectId(userId, projectId);
        Optional.ofNullable(ListUtil.emptyListToNull(taskList))
                .ifPresent(list -> list.forEach(task -> taskRepository.deleteById(userId, task.getId())));
        projectRepository.deleteById(userId, projectId);
    }

}
