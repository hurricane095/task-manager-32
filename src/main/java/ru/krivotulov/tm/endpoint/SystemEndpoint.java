package ru.krivotulov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.krivotulov.tm.api.endpoint.ISystemEndpoint;
import ru.krivotulov.tm.api.service.IPropertyService;
import ru.krivotulov.tm.api.service.IServiceLocator;
import ru.krivotulov.tm.dto.request.ServerAboutRequest;
import ru.krivotulov.tm.dto.request.ServerVersionRequest;
import ru.krivotulov.tm.dto.response.ServerAboutResponse;
import ru.krivotulov.tm.dto.response.ServerVersionResponse;

/**
 * SystemEndPoint
 *
 * @author Aleksey_Krivotulov
 */
public class SystemEndpoint implements ISystemEndpoint {

    @NotNull
    private final IServiceLocator serviceLocator;

    public SystemEndpoint(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    @NotNull
    public ServerAboutResponse getAbout(@NotNull final ServerAboutRequest serverAboutRequest) {
        @NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
        @NotNull final ServerAboutResponse response = new ServerAboutResponse();
        response.setEmail(propertyService.getAuthorEmail());
        response.setName(propertyService.getAuthorName());
        return response;
    }

    @Override
    @NotNull
    public ServerVersionResponse getVersion(@NotNull final ServerVersionRequest serverVersionRequest) {
        @NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
        @NotNull final ServerVersionResponse response = new ServerVersionResponse();
        response.setVersion(propertyService.getApplicationVersion());
        return response;
    }

}
