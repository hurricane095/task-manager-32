package ru.krivotulov.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.krivotulov.tm.api.component.ISaltProvider;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * HashUtil
 *
 * @author Aleksey_Krivotulov
 */
public interface HashUtil {

    @Nullable
    static String salt(
            @Nullable final ISaltProvider saltProvider,
            @Nullable final String value
    ) {
        if (saltProvider == null) return null;
        @NotNull final String secret = saltProvider.getPasswordSecret();
        @NotNull final Integer iteration = saltProvider.getPasswordIteration();
        return salt(value, secret, iteration);
    }

    @Nullable
    static String salt(@Nullable final String password,
                       @Nullable final String secret,
                       @Nullable final Integer iteration
    ) {
        if (password == null || secret == null || iteration == null) return null;
        @Nullable String result = password;
        for (int i = 0; i < iteration; i++) {
            result = md5(secret + result + secret);
        }
        return result;
    }

    @Nullable
    static String md5(@Nullable final String val) {
        if (val == null) return null;
        try {
            @NotNull MessageDigest md = MessageDigest.getInstance("MD5");
            @NotNull final byte[] array = md.digest(val.getBytes());
            @NotNull final StringBuffer stringBuffer = new StringBuffer();
            for (int i = 0; i < array.length; i++) {
                stringBuffer.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1, 3));
            }
            return stringBuffer.toString();
        } catch (@NotNull final NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

}
