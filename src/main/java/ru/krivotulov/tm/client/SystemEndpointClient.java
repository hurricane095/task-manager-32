package ru.krivotulov.tm.client;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.krivotulov.tm.api.endpoint.ISystemEndpoint;
import ru.krivotulov.tm.dto.request.ServerAboutRequest;
import ru.krivotulov.tm.dto.request.ServerVersionRequest;
import ru.krivotulov.tm.dto.response.ServerAboutResponse;
import ru.krivotulov.tm.dto.response.ServerVersionResponse;

/**
 * SystemEndpointClient
 *
 * @author Aleksey_Krivotulov
 */
public final class SystemEndpointClient extends AbstractEndpoint implements ISystemEndpoint {

    @NotNull
    @Override
    @SneakyThrows
    public ServerAboutResponse getAbout(@NotNull ServerAboutRequest request) {
        return (ServerAboutResponse) call(request);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ServerVersionResponse getVersion(@NotNull ServerVersionRequest request) {
        return (ServerVersionResponse) call(request);
    }

    @SneakyThrows
    public static void main(String[] args) {
        final SystemEndpointClient client = new SystemEndpointClient();
        client.connect();
        final ServerAboutResponse aboutResponse = client.getAbout(new ServerAboutRequest());
        final ServerVersionResponse versionResponse = client.getVersion(new ServerVersionRequest());
        System.out.println(aboutResponse.getName());
        System.out.println(aboutResponse.getEmail());
        System.out.println(versionResponse.getVersion());
        client.disconnect();
    }

}
