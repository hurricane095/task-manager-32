package ru.krivotulov.tm.task;

import org.jetbrains.annotations.NotNull;
import ru.krivotulov.tm.component.Server;

import java.net.Socket;

/**
 * AbstractServerSocketTask
 *
 * @author Aleksey_Krivotulov
 */
public abstract class AbstractServerSocketTask extends AbstractServerTask {

    @NotNull
    protected final Socket socket;

    public AbstractServerSocketTask(@NotNull final Server server, @NotNull final Socket socket) {
        super(server);
        this.socket = socket;
    }

}
