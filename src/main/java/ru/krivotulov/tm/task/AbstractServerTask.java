package ru.krivotulov.tm.task;

import org.jetbrains.annotations.NotNull;
import ru.krivotulov.tm.component.Server;

/**
 * AbstractServerTask
 *
 * @author Aleksey_Krivotulov
 */
public abstract class AbstractServerTask implements Runnable {


    @NotNull
    protected final Server server;

    public AbstractServerTask(@NotNull final Server server) {
        this.server = server;
    }

}
