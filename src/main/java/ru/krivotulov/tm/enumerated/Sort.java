package ru.krivotulov.tm.enumerated;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.krivotulov.tm.comparator.CreatedComparator;
import ru.krivotulov.tm.comparator.DateBeginComparator;
import ru.krivotulov.tm.comparator.NameComparator;
import ru.krivotulov.tm.comparator.StatusComparator;

import java.util.Comparator;

public enum Sort {

    BY_NAME("Sort by name", NameComparator.INSTANCE),
    BY_STATUS("Sort by status", StatusComparator.INSTANCE),
    BY_CREATED("Sort by created", CreatedComparator.INSTANCE),
    BY_DATE_BEGIN("Sort by date begin", DateBeginComparator.INSTANCE);

    @NotNull
    private final String displayName;

    @NotNull
    private final Comparator comparator;

    @Nullable
    public static Sort toSort(@Nullable final String value) {
        if (value == null || value.isEmpty()) return null;
        for (Sort sort : values()) {
            if (sort.name().equals(value)) return sort;
        }
        return null;
    }

    Sort(@NotNull String displayName, @NotNull Comparator comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

    @NotNull
    public String getDisplayName() {
        return displayName;
    }

    @NotNull
    public Comparator getComparator() {
        return comparator;
    }

}
