package ru.krivotulov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

/**
 * AbstractOwner
 *
 * @author Aleksey_Krivotulov
 */
@Getter
@Setter
@NoArgsConstructor
public class AbstractUserOwnedModel extends AbstractModel {

    @NotNull
    protected String userId;

}
