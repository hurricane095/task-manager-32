package ru.krivotulov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.krivotulov.tm.util.TerminalUtil;

/**
 * TaskUpdateByIdCommand
 *
 * @author Aleksey_Krivotulov
 */
public class TaskUpdateByIdCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task-update-by-id";

    @NotNull
    public static final String DESCRIPTION = "Update task by id.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE TASK BY ID]");
        System.out.println("ENTER ID: ");
        @Nullable final String id = TerminalUtil.readLine();
        System.out.println("ENTER NAME: ");
        @Nullable final String name = TerminalUtil.readLine();
        System.out.println("ENTER DESCRIPTION: ");
        @Nullable final String description = TerminalUtil.readLine();
        @NotNull final String userId = getUserId();
        getTaskService().updateById(userId, id, name, description);
    }

}
