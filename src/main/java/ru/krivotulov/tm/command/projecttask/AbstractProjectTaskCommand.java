package ru.krivotulov.tm.command.projecttask;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.krivotulov.tm.api.service.IAuthService;
import ru.krivotulov.tm.api.service.IProjectTaskService;
import ru.krivotulov.tm.command.AbstractCommand;
import ru.krivotulov.tm.enumerated.Role;

/**
 * AbstractProjectTaskCommand
 *
 * @author Aleksey_Krivotulov
 */
public abstract class AbstractProjectTaskCommand extends AbstractCommand {

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    public IProjectTaskService getProjectTaskService() {
        return serviceLocator.getProjectTaskService();
    }

    @NotNull
    public IAuthService getAuthService() {
        return serviceLocator.getAuthService();
    }

    @NotNull
    public String getUserId(){
        return getAuthService().getUserId();
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
