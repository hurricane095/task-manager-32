package ru.krivotulov.tm.command.projecttask;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.krivotulov.tm.util.TerminalUtil;

/**
 * BindTaskToProjectCommand
 *
 * @author Aleksey_Krivotulov
 */
public class BindTaskToProjectCommand extends AbstractProjectTaskCommand {

    @NotNull
    public static final String NAME = "bind-task-to-project";

    @NotNull
    public static final String DESCRIPTION = "Bind task to project.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[BIND TASK TO PROJECT]");
        System.out.println("ENTER PROJECT ID: ");
        @Nullable final String projectId = TerminalUtil.readLine();
        System.out.println("TASK ID: ");
        @Nullable final String taskId = TerminalUtil.readLine();
        @NotNull final String userId = getUserId();
        getProjectTaskService().bindTaskToProject(userId, projectId, taskId);
    }

}
