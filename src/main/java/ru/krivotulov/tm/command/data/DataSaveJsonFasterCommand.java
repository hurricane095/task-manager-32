package ru.krivotulov.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.krivotulov.tm.dto.Domain;

import java.io.FileOutputStream;

/**
 * DataSaveJsonFasterCommand
 *
 * @author Aleksey_Krivotulov
 */
public class DataSaveJsonFasterCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-save-json-faster";

    @NotNull
    public static final String DESCRIPTION = "Save data in json file faster.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA JSON FASTER SAVE]");
        @NotNull final Domain domain = getDomain();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
        @NotNull FileOutputStream fileOutputStream = new FileOutputStream(FILE_FASTER_JSON);
        fileOutputStream.write(json.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();
    }

}
