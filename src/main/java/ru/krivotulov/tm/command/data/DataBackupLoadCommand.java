package ru.krivotulov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.krivotulov.tm.dto.Domain;

import java.io.ByteArrayInputStream;
import java.io.ObjectInputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Base64;

/**
 * DataBackupLoadCommand
 *
 * @author Aleksey_Krivotulov
 */
public class DataBackupLoadCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "backup-load";

    @NotNull
    public static final String DESCRIPTION = "Load backup from base64 file.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    public void execute() {
        @Nullable final byte[] base64Date = Files.readAllBytes(Paths.get(FILE_BACKUP));
        @Nullable final byte[] bytes = Base64.getDecoder().decode(base64Date);
        @NotNull final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes);
        @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
        @NotNull final Domain domain = (Domain) objectInputStream.readObject();
        objectInputStream.close();
        byteArrayInputStream.close();
        setDomain(domain);
    }

}
