package ru.krivotulov.tm.command.data;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.krivotulov.tm.dto.Domain;

import java.io.FileOutputStream;

/**
 * DataSaveXmlFasterCommand
 *
 * @author Aleksey_Krivotulov
 */
public class DataSaveXmlFasterCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-save-xml-faster";

    @NotNull
    public static final String DESCRIPTION = "Save data in xml file faster.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA XML FASTER SAVE]");
        @NotNull final Domain domain = getDomain();
        @NotNull final XmlMapper xmlMapper = new XmlMapper();
        @NotNull final String json = xmlMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
        @NotNull FileOutputStream fileOutputStream = new FileOutputStream(FILE_FASTER_XML);
        fileOutputStream.write(json.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();
    }

}
