package ru.krivotulov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.krivotulov.tm.dto.Domain;

import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * DataBinarySaveCommand
 *
 * @author Aleksey_Krivotulov
 */
public final class DataBinarySaveCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-save-bin";

    @NotNull
    public static final String DESCRIPTION = "Save data in binary file.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA BINARY SAVE]");
        @NotNull final Domain domain = getDomain();
        @NotNull final File file = new File(FILE_BINARY);
        @NotNull final Path path = file.toPath();
        Files.deleteIfExists(path);
        Files.createFile(path);
        @NotNull FileOutputStream fileOutputStream = new FileOutputStream(file);
        @NotNull ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
        objectOutputStream.writeObject(domain);
        objectOutputStream.close();
        fileOutputStream.close();
    }

}
