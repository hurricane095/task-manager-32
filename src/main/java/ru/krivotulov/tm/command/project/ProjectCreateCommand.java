package ru.krivotulov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.krivotulov.tm.util.TerminalUtil;

import java.util.Date;

/**
 * ProjectCreateCommand
 *
 * @author Aleksey_Krivotulov
 */
public class ProjectCreateCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = "project-create";

    @NotNull
    public static final String DESCRIPTION = "Create new project.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT CREATE]");
        System.out.println("ENTER NAME: ");
        @Nullable final String name = TerminalUtil.readLine();
        System.out.println("ENTER DESCRIPTION: ");
        @Nullable final String description = TerminalUtil.readLine();
        System.out.println("ENTER DATE BEGIN: ");
        @Nullable final Date dateBegin = TerminalUtil.nextDate();
        System.out.println("ENTER DATE END: ");
        @Nullable final Date dateEnd = TerminalUtil.nextDate();
        @NotNull final String userId = getUserId();
        getProjectService().create(userId, name, description, dateBegin, dateEnd);
    }

}
