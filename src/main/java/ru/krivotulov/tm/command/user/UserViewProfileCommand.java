package ru.krivotulov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.krivotulov.tm.enumerated.Role;
import ru.krivotulov.tm.model.User;

/**
 * UserViewProfileCommand
 *
 * @author Aleksey_Krivotulov
 */
public final class UserViewProfileCommand extends AbstractUserCommand {

    @NotNull
    public static final String NAME = "user-show-profile";

    @NotNull
    public static final String DESCRIPTION = "User show profile.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        @NotNull final User user = getAuthService().getUser();
        showUser(user);
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
