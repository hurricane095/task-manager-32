package ru.krivotulov.tm.api.component;

import org.jetbrains.annotations.NotNull;

/**
 * ISaltProvider
 *
 * @author Aleksey_Krivotulov
 */
public interface ISaltProvider {

    @NotNull
    Integer getPasswordIteration();

    @NotNull
    String getPasswordSecret();

}
