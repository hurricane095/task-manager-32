package ru.krivotulov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.krivotulov.tm.enumerated.Status;
import ru.krivotulov.tm.model.Project;

import java.util.Date;

public interface IProjectService extends IUserOwnedService<Project> {

    Project create(@Nullable String userId, @Nullable String name);

    Project create(@Nullable String userId, @Nullable String name, @Nullable String description);

    Project create(@Nullable String userId,
                   @Nullable String name,
                   @Nullable String description,
                   @Nullable Date dateBegin,
                   @Nullable Date dateEnd);

    Project updateById(@Nullable String userId,
                       @Nullable String id,
                       @Nullable String name,
                       @Nullable String description);

    Project updateByIndex(@Nullable String userId,
                          @Nullable Integer index,
                          @Nullable String name,
                          @Nullable String description);

    Project changeStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    Project changeStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status);


}
