package ru.krivotulov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.krivotulov.tm.dto.request.ServerAboutRequest;
import ru.krivotulov.tm.dto.request.ServerVersionRequest;
import ru.krivotulov.tm.dto.response.ServerAboutResponse;
import ru.krivotulov.tm.dto.response.ServerVersionResponse;

/**
 * ISystemEndpoint
 *
 * @author Aleksey_Krivotulov
 */
public interface ISystemEndpoint {

    @NotNull
    ServerAboutResponse getAbout(@NotNull ServerAboutRequest serverAboutRequest);

    @NotNull
    ServerVersionResponse getVersion(@NotNull ServerVersionRequest serverVersionRequest);

}
